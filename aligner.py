#!/usr/bin/env python3
import sys
def print_usage():
	print('Usage for script:')
	print('python aligner.py -i "<input>.mp3" -o "<output_directory>" -n "<output_name>.mp3"')
	print('Note, the quotes around specified inputs are only required if the string has a space in it.')
	print('The only mandatory flag is "-i". "-i" can specify either a mp3 file or an entire directory. If a directory is specified then the script will search for all mp3s in the directory and ignore everything else. No need for "*.mp3"')
	print('If "-o" is not used, the script will default to the output directory that is set in the config file.')
	print('The "-n" flag will only be used if input is a mp3 file. It is used to set the name of the output mp3 file. If it is not set then the same name as the input is used.')


if len(sys.argv) == 1 and __name__ == '__main__':
	print_usage()
	sys.exit(1)

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import time
import os
import numpy as np
import pickle
import logging
import threading
import subprocess
import shutil
import fractions
import decimal
import signal
import argparse
import configparser
GObject.threads_init()
Gst.init(None)



# These two exceptions as defined to make the logging process easier
# as we can take the exception object and convert it to a string with something like 'Error: %s' % <exception_object>

class FileNotFound(Exception):
	def __str__(self):
		return "File %s doesn't exist" % self.file

class StreamError(Exception):
	def __str__(self):
		return "Gstreamer communicated error: %s" % self.error


class Gstreamer:
	# This class is used to wrap all the gstreamer functionality that we need.
	def __init__(self):
		self.logger = None
		self.temp_out = None
		self.max_bytes = None
		self.conv_type_dec = None
		self.conv_format_dec = None
		self.conv_channels_dec = None
		self.conv_frame_rate = None
		self.msg = None
		return



	def init_decode(self,file):

		# This function initilizes the gstreamer pipeline to decode
		# an mp3 to raw audio so the data can be analyzed. The 'file'
		# variable is the path of the mp3 file that needs to be decoded.
		# self.raw is the path to the output raw audio on the disk.


		GObject.threads_init()
		self.raw = os.path.join(self.temp_dir,self.temp_out)
		if not os.path.isfile(file):
			err = FileNotFound()
			err.file = file
			raise err

		if not os.path.isdir(self.temp_dir):
			os.mkdir(self.temp_dir)

		self.mainloop = GObject.MainLoop()
		if self.logger:
			self.logger.info('Initializing Gstreamer decoding pipeline')

		# The Gst.parse_launch is the easiest way to get gstreamer to decode the mp3 file.
		# The usage is exactly the same as the command line gstreamer.
		# gstreamer usage splits up the steps of the process specified using '!'
		# Here are the steps for this script:

		# 'filesrc location = %s' refers to the input file to be converted.
		# 'decodebin' specifies that the file will be decoded.
		# 'audioconvert' specifies that the decoded audio will be converted.
		# 'capsfilter caps = "%s,format=%s,channels=%s,rate=%s' specifies the parameters of the conversion.
		# 	In this case we want the format to stay raw, the only thing we care about is that the number of channels
		# 	is set to 1 and that the output is signed 16 bit integers(the endianess can also be specified). 
		# 	All the options can be found here: https://gstreamer.freedesktop.org/documentation/design/mediatype-audio-raw.html
		# 'filesink location = "%s"' specifies the location of the output.




		self.pipeline = Gst.parse_launch('''filesrc location = %s !
										 decodebin ! audioconvert !
										 capsfilter caps = "%s,format=%s,channels=%s,rate=%s" ! filesink location = "%s"
										''' % (
										 file, 
										 self.conv_type_dec, 
										 self.conv_format_dec, 
										 self.conv_channels_dec, 
										 self.conv_frame_rate, 
										 self.raw))
		self.bus = self.pipeline.get_bus()
		self.bus.add_signal_watch()
		# This sets the function 'self.message_handler' to run
		# be called if any message is sent from gstreamer.
		self.bus.connect("message", self.message_handler)
		self.encode = False


	def init_encode(self,file,out):
		# This function is very similar to 'init_decode'
		# This function is actually deprecated. It was orginally used to
		# encode the raw audio back to mp3 once the position of the marker is found.
		# However, this results in a loss in quality and mp3splt/wrap is now used instead,
		# so there is no need to encode audio anymore.
		GObject.threads_init()

		if not os.path.isfile(file):
			err = FileNotFound()
			err.file = file
			raise err

		self.mainloop = GObject.MainLoop()
		if self.logger:
			self.logger.info('Initializing Gstreamer encoding pipeline')
		self.pipeline = Gst.parse_launch('''filesrc location = %s !
										 capsfilter caps = "%s,format=%s,channels=%s,rate=%s" ! 
										 audioconvert !
										 lamemp3enc !
										 filesink location = "%s"
										 ''' % (
										 file, 
										 self.conv_type_dec, 
										 self.conv_format_dec, 
										 self.conv_channels_dec, 
										 self.conv_frame_rate, 
										 out))
		self.bus = self.pipeline.get_bus()
		self.bus.add_signal_watch()
		self.bus.connect("message", self.message_handler)
		self.encode = True



	def message_handler(self,bus, message):
		# Here we handle all the messages sent from gstreamer.
		# Note, this function is run asynchronously when called by gstreamer.
		# The different types of messages can be found here https://valadoc.org/gstreamer-1.0/Gst.MessageType.html
		# Since all we are using gstreamer for is decoding there are only 2 types of messages we care about.
		# Type 1 means the deccoding is finished, and Type 2 means there was some sort of error.

		msgType = message.type
		if msgType == Gst.MessageType(1):
			self.msg = 'EOS'
			# Free Resources
			self.pipeline.set_state(Gst.State.NULL)
			return

		elif msgType == Gst.MessageType(2):
			self.msg = 'ERROR'
			self.err_msg = msgType
			# Free Resources
			self.pipeline.set_state(Gst.State.NULL)
			return




	def decode_src_created(self, element, pad):
		pad.link(self.sink.get_static_pad("sink"))
		
	def run(self):
		# This function is called when gstreamer needs to actually begin the docoding process.
		# To do this the pipeline state must be set to 'Gst.State.PLAYING' and since PyGObject
		# utilizes multiple threads, we need to dedicate a thread to running the gst mainloop.
		# 'signal.signal(signal.SIGINT, signal.SIG_DFL)' needs to be added so that keyboard breaks
		# are sent to the child processes. 


		if self.logger:
			if not self.encode:
				self.logger.info('Converting mp3 to raw...')
			else:
				self.logger.info('Converting raw to mp3...')
		self.pipeline.set_state(Gst.State.PLAYING)
		self.thread = threading.Thread(target=self.mainloop.run)
		self.thread.setDaemon(True)
		self.thread.start()
		signal.signal(signal.SIGINT, signal.SIG_DFL)

		# Here we loop forever until gstreamer is finished or an error is communicated.
		# In case of error, we raise an exception so that it can be logged.
		# self.mainloop.quit() quits the thread dedicated by PyGObject. If this is not
		# run that script will hang after completion as the thread will be considered
		# running.

		while True:
			time.sleep(1)
			if self.msg:
				if self.msg == 'EOS':
					if self.logger:
						self.logger.info('Finished!')
					self.mainloop.quit()
					return

				else:
					err = StreamError()
					err.error = self.err_msg
					self.mainloop.quit()
					raise err






class Aligner(object):
	# This class wraps all of the alignment logic as well as logging and file I/O.


	logger = logging.getLogger('Aligner')
	logger.setLevel(logging.INFO)
	handler = logging.FileHandler('log.txt')
	handler.setLevel(logging.INFO)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	handler.setFormatter(formatter)
	logger.addHandler(handler)
	stdout = logging.StreamHandler(sys.stdout)
	stdout.setLevel(logging.DEBUG)
	stdout.setFormatter(formatter)
	logger.addHandler(stdout)
	config = 'config'
	def __init__(self):
		self.compression_value = None
		self.conv_type_dec = None
		self.conv_type_dec = None
		self.conv_format_dec = None
		self.conv_channels_dec = None
		self.conv_frame_rate = None
		self.g_temp_out = None
		self.temp_dir = None
		self.load_config()
		return


	def load_config(self):

		config_parser = configparser.ConfigParser()
		config_parser.read(self.config)
		for key,value in config_parser['DEFAULT'].items():
			self.__dict__[key] = value

		self.compression_value = int(self.compression_value)

		if self.marker_file in ('',' '):
			self.logger.info('ERROR: marker_file not set in config, exiting...')
			sys.exit(1)


		if not os.path.isdir(self.temp_dir):
			os.mkdir(self.temp_dir)





	def get_gstreamer(self):
		# Here we creat a Gstreamer class and set all of the
		# import values that are stored in the config file.
		G = Gstreamer()
		G.logger = self.logger
		G.conv_type_dec = self.conv_type_dec
		G.conv_format_dec = self.conv_format_dec
		G.conv_channels_dec = self.conv_channels_dec
		G.conv_frame_rate = self.conv_frame_rate
		G.temp_out = self.g_temp_out
		G.temp_dir = self.temp_dir
		G.compression_value = self.compression_value
		return G


	def decode_mp3(self, mp3_file, marker=False):
		# Here we have the Gstreamer object decode the mp3_file



		G = self.get_gstreamer()

		G.init_decode(mp3_file)
		G.run()
		if marker:
			# 'np.fromstring(self.np_marker,dtype="int16")' loads in the gstreamer output and returns a numpy array.
			# The [::self.compression_value] only keeps every other self.compression_value-th index.
			# For example, if self.compression_value = 100, we only keep index 0, 100, 200, 300...
			# This optimizes the memory and speed usage but doesn't impact performance.
			# Converting the array to type 'float32' makes the analysis 5x faster. 
			self.np_marker = open(G.raw,'rb').read()
			os.remove(G.raw)
			self.np_marker = np.fromstring(self.np_marker, dtype='int16')[::self.compression_value].astype(np.float32)
			del G
			return

		else:
			self.stream = open(G.raw,'rb').read()
			self.stream_len = len(self.stream)
			os.remove(G.raw)
			self.stream = np.fromstring(self.stream, dtype='int16')[::self.compression_value].astype(np.float32)
			del G
			return

	def encode_mp3(self, raw, out):
		G = self.get_gstreamer()
		G.init_encode(raw, out)
		G.run()


		del G
		return


	def align(self):

		# This function contains all the logic to find the marker within the stream.

		start_time = time.time()
		self.logger.info('Beginning Initial Analysis')


		# Here we loop through all windows of size np_marker in the stream.
		# Ex. assume stream = [0,0,0,0,0,0,0,0,0,0] and marker = [0,0,0]
		# indices used in loop will contain 1 instead of 0.
		# Loop 1:
		# 	stream: [1,1,1,0,0,0,0,0,0,0]
		#	marker: [0,0,0]
		# Loop 2:
		# 	stream: [0,1,1,1,0,0,0,0,0,0]
		#	marker:   [0,0,0]
		# We shift over until we hit the end of the stream array.
		#
		# As the name suggests, np.correlate will correlate the marker array and
		# the current window of the stream array. A large positive number means the two signals
		# rise and fall with eachother. A large negative number means when the marker signal rises, the stream
		# signal falls, and vice versa. When the number is close to 0 we say the two signals are uncorrelated.
		# From this, we want to know when the stream and the marker have high-positive correlational values as
		# this would suggest the two audio signals are similar because of the intrinsic complicated nature of audio signals.
		# This script was designed for a stream where the marker shows up multiple times so we should expect multiple
		# peaks in the correlation. We store the correlation and the index in a sorted list.



		diff = self.stream.size - self.np_marker.size
		max_corr = [(0,0)]*10
		for i in range(diff):
			# np.correlate returns a 2d array with one value, so we flatten the output and extract the value.
			corr = np.correlate(self.np_marker, self.stream[i:i+self.np_marker.size]).flatten()[0]


			# Since the list is sorted small -> large, we only need to check the 0th index to see if the
			# current index (i) needs to be added to the list.
			if corr > max_corr[0][0]:
				max_corr[0] = (corr,i)
				max_corr.sort(key = lambda x: x[0])










		top3 = max_corr[::-1][:3]
		silent_lengths = []
		t = time.time() - start_time
		self.logger.info('Finished Initial Analysis, took %.2fs' % t)
		self.logger.info('Found potential candidates, conducting secondary analysis')

		# Since this script was designed for a stream that contains the marker multiple times, we need to conduct a further
		# analysis in order to determine the appropriate starting point. The particular stream this was designed for had 
		# about 15k mp3 frames of silence before the marker. For the secondary analysis, we look around a particular index
		# with a high correlation, and find the largets number of 0s in a row. For example the list [0,1,0,0,0,5,0,0] has 3 as
		# the largest number of 0s that occur in a row. The code to find this value is trivial, but in this case we can use
		# numpy's wonderful vectorized operations to really speed up the process. The variable 'idx_pairs' will be of shape (1,N)
		# where N is the number of substrings with all 0s that occur in the window. For each vector in 'idx_pairs,' the 0th index
		# will contain the starting point and the 1st index contains the ending point. So we just need to compute the row-wise
		# difference in the array and find the max value. 

		for _,i in top3:

			window = self.stream[i-100:i+100]
			idx_pairs = np.where(np.diff(np.hstack(([False],window==0,[False]))))[0].reshape(-1,2)
			if len(idx_pairs) == 0:
				continue
			diff = np.diff(idx_pairs,axis=1)
			silent_lengths.append((diff.max(),i))

		
		
		if len(silent_lengths) == 0:
			self.logger.info('Warning! Secondary analysis failed to find a candidate. Defaulting to max probability from inital analysis')
			pos = top3[0][-1]
		else:
			pos = max(silent_lengths, key = lambda x: x[0])[-1]

		# Once the position is found we call the generate output function to actually generate the new mp3.



		self.generate_output((pos,self.stream_len,self.stream.size))
		del self.np_marker
		del self.stream


	def check_subprocess_error(self, output, mp3wrap=False):
		if output.returncode and output.returncode != 0:
			if mp3wrap:
				source = 'mp3wrap'
			else:
				source = 'mp3splt'


			self.logger.info("%s exited with return code %i" % (source,output.returncode))
			sys.exit(1)



		
	def generate_output(self, args):

		# We will use mp3splt/wrap to align the stream to the appropriate position.
		# mp3splt needs a time value to split an mp3 value. The position ('pos') that was found
		# is the index in the raw audio array we want to set as the beginning. Since we know the
		# bit rate of the mp3, we can fairly accurately convert 'pos' into a 'mm.ss.hh' format where 'hh'
		# stands for hundreths of a second. We use the Python Decimal library to avoid floating point errors. 
		# Since the raw audio can be on the order of 10**9 bytes long, floating point errors are noticable
		# in this case. 


		pos, stream_len, cpr_len = args

		devnull = open(os.devnull,'w')

		num = decimal.Decimal(pos*stream_len) / decimal.Decimal(cpr_len)
		secs = num / decimal.Decimal(2*44100)

		minutes = secs // 60
		diff = secs - 60*minutes
		seconds = int(diff)
		diff -= seconds
		hundreths = '%.2f' % diff


		splt_time = '%i.%i.%s' % (minutes, seconds, hundreths)

		temp_dir = self.temp_dir
		command = ["mp3splt","-f","-d", temp_dir, self.misaligned, splt_time, "EOF"]
		otpt = subprocess.Popen(command, stdout=devnull, stderr=devnull)
		_,self.err = otpt.communicate()
		self.check_subprocess_error(otpt)
		fn1 = os.listdir(temp_dir)[0]
		fn1 = os.path.join(temp_dir,fn1)
		command = ["mp3splt","-f","-d", temp_dir, self.misaligned, "0.0.0", splt_time]
		otpt = subprocess.Popen(command, stdout=devnull, stderr=devnull)
		_,self.err = otpt.communicate()
		self.check_subprocess_error(otpt)
		for fn in os.listdir(temp_dir):
			if os.path.join(temp_dir,fn) == fn1:
				continue
			fn2 = os.path.join(temp_dir,fn)
		os.rename(fn1,os.path.join(temp_dir,'1.mp3'))
		os.rename(fn2,os.path.join(temp_dir,'2.mp3'))


		fname = self.out_name
		fname = os.path.join(self.aligned_out_dir,fname)
		command = ["mp3wrap", fname, "%s1.mp3" % temp_dir, "%s2.mp3" % temp_dir]
		otpt = subprocess.Popen(command, stdout=devnull, stderr=devnull)
		_,self.err = otpt.communicate()
		self.check_subprocess_error(otpt, mp3wrap=True)
		mp3wrap_name = fname.split('.mp3')[0] + '_MP3WRAP.mp3'
		os.rename(mp3wrap_name,fname)

		with open(fname,'rb') as fread:
			data = fread.read()
			ind = data.find(0000)
			if ind == -1:
				ind = 0
			data = data[ind:]
			fread.close()
		open(fname,'wb').write(data)
		self.logger.info('Aligned MP3 written to %s' % fname)
		







	def main(self, misaligned, outfile=None,outdir=None):
		if not os.path.isdir(self.aligned_out_dir):
			os.mkdir(self.aligned_out_dir)

		if not outfile:
			self.out_name = misaligned.split('/')[-1]
		else:
			self.out_name = outfile

		if outdir:
			self.aligned_out_dir = outdir




		self.logger.info('Starting alignment for file %s' % misaligned)
		self.misaligned = misaligned
		self.decode_mp3(self.marker_file,marker=True)
		self.decode_mp3(misaligned)
		self.align()
		if os.path.isdir(self.temp_dir):
			shutil.rmtree(self.temp_dir)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Stream Aligner')
	parser.add_argument('-i', action="store", dest="i")
	parser.add_argument('-o', action="store", dest="o")
	parser.add_argument('-n', action="store", dest="n")

	namespace = parser.parse_args(sys.argv[1:])
	args = {}
	if namespace.i:
		args['-i'] = namespace.i
	if namespace.o:
		args['-o'] = namespace.o
	if namespace.n:
		args['-n'] = namespace.n


	if not '-i' in args.keys():
		print_usage()
		sys.exit(1)


	input = args['-i']
	outdir = None
	outfile = None
	if os.path.isfile(input):
		if '-o' in args.keys():
			outdir = args['-o']
			if not os.path.isdir(outdir):
				os.mkdir(outdir)

		if '-n' in args.keys():
			outfile = args['-n']

		align = Aligner()
		align.main(input,outfile=outfile,outdir=outdir)



	elif os.path.isdir(input):
		mp3s = []
		for fn in os.listdir(input):
			if fn.endswith('.mp3'):
				mp3s.append(os.path.join(input,fn))
		if len(mp3s) == 0:
			print('No mp3 files were found in specified directory, exiting...')
			sys.exit(1)

		outdir = None
		if '-o' in args.keys():
			outdir = args['-o']
			if not os.path.isdir(outdir):
				os.mkdir(outdir)
		for mp3 in mp3s:
			align = Aligner()
			align.main(mp3,outdir=outdir)

	else:
		print('%s is neither a file nor a directory, exiting...')
		sys.exit(1)
