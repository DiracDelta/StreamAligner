#!/usr/bin/env python3
import sys
import os


def print_usage():
	print('Usage for file(s):')
	print('	python3 remove_id3.py -f file.mp3 file2.mp3...')
	print('Usage for directory(s):')
	print('	python3 remove_id3.py -d directory/ directory2/ ...')
	print('For directory usage all mp3 files in directories will have id3 tags removed.')



def remove_id3(file):
	data = open(file,'rb').read()
	ind = data.find(0000)
	if ind == -1:
		print('id3 tags for file %s was not found, skipping file.' % file)
		return

	data = data[ind:]
	open(file,'wb').write(data)
	print('Successfully removed id3 tags for file %s' % file)




if __name__ == '__main__':
	args = sys.argv[1:]
	
	if len(args) < 2:
		print_usage()
		sys.exit(-1)

	if '-f' == args[0]:
		files = ' '.join(args[1:]).split('.mp3')
		for i,fn in enumerate(files):
			files[i] = fn + '.mp3'
		while '.mp3' in files:
			files.remove('.mp3')
		print(files)



	elif '-d' == args[0]:
		files = []
		for di in args[1:]:
			for file in os.listdir(di):
				if file.endswith('.mp3'):
					files.append(os.path(di,file))


	# for file in files:
	# 	remove_id3(file)


	