# StreamAligner


If you use [AudioStreamRecorder](https://github.com/RiemanZeta/AudioStreamRecorder) to record a stream, chances are that the beginning of the mp3 will not coincide with the beginning of the stream. If the stream has a jingle that plays at the beginning, then this script can be used to align the mp3 to the beginning of the stream. A mp3 of just the jingle is required to perform the analysis. You can use audacity to isolate the jingle. 

Please look at the config for the important parameters.

Requirements:
* Numpy
* gstreamer(with mp3 codecs)
* pygobject
* mp3splt and mp3wrap


