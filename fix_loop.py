#!/usr/bin/env python3
import os
import sys


def fix_loop(file,outdir = None, overwrite = False):
	if overwrite:
		outdir = file

	else:
		if not outdir:
			if not os.path.isdir('Delooped_Streams/'):
				os.mkdir('Delooped_Streams/')
			output = 'Delooped_Streams/' + file.split('/')[-1]

		else:
			output = outdir

	cmp_length = 45
	byte_rate = 128000 // 8
	with open(file,'rb') as f:
		beg = f.read(cmp_length*byte_rate)
		data = f.read()
		f.close()


	location = data.find(beg)
	if location == -1:
		print("Couldn't find loop for file %s" % file)
		file = file.split('/')[-1]
		with open(output,'wb') as f:
			f.write(beg + data)
			f.close()
			return


	location +=  cmp_length*byte_rate

	full = beg + data
	full = full[:location]
	file = file.split('/')[-1]
	with open(output,'wb') as f:
		f.write(full)
		f.close()
		return

if __name__ == '__main__':

	if not len(sys.argv) in (2,3):
		print('Usage for single mp3: python3 fix_loop.py path/to/stream.mp3')
		print('Usage for directory of mp3s: python3 fix_loop.py path/to/dir path/to/outdir')
		sys.exit(1)

		
	if len(sys.argv) == 2:
		_,path = sys.argv
		if os.path.isfile(path) and path.endswith('.mp3'):
			fix_loop(path)
	else:
		_,path,out = sys.argv
		if not os.path.isdir(out):
			os.mkdir(out)
		elif os.path.isdir(path):
			mp3s = []
			for fn in os.listdir(path):
				if fn.endswith('.mp3'):
					mp3s.append(os.path.join(path,fn))
			for mp3 in mp3s:
				fix_loop(mp3)